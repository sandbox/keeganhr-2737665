/**
 * @file
 * Gulp automation workflow.
 */

'use strict';

// Require the needed plugins.
var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var browsersync = require('browser-sync').create();
var sourcemaps = require('gulp-sourcemaps');
var template = require('gulp-template');
var inquirer = require('inquirer');
var install = require('gulp-install');
var replace = require('gulp-replace');
var config = require('./config.json');
var rename = require('gulp-rename');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var del = require('del');
var fs = require('fs');
var _ = require('underscore.string');

// Process styles.
gulp.task('styles', function () {
  return gulp.src('scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(autoprefixer('last 2 versions', 'ie > 9'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('css'))
    .pipe(browsersync.reload({
      stream: true
    }));
});

// Process scripts.
gulp.task('scripts', function () {
  return gulp.src('js/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default', {verbose: true}));
});

// Sync browsers.
gulp.task('browsersync', function () {
  browsersync.init({
    proxy: config.localURL
  });
});

// Setup watch task.
gulp.task('watch', ['browsersync', 'styles', 'scripts'], function () {
  gulp.watch('scss/**/*.scss', ['styles']);
  gulp.watch('js/**/*.js', ['scripts', browsersync.reload]);
  gulp.watch('images/**/*.+(png|jpg|jpeg|gif|svg)', browsersync.reload);
  gulp.watch('templates/**/*.twig', browsersync.reload);
  gulp.watch('*.yml', browsersync.reload);
});

// Setup default task.
gulp.task('default', ['watch']);

// Setup init task.
gulp.task('init', function (done) {
  inquirer.prompt([
    {
      type: 'input',
      name: 'name',
      message: 'What do you want to name your theme?',
      default: 'Theme Name'
    }, {
      type: 'input',
      name: 'devurl',
      message: 'What is your local development url?',
      default: 'localhost:8888'
    }
  ]).then(function (answers) {
    answers.nameDashed = _.slugify(answers.name);
    answers.machineName = _.underscored(answers.nameDashed);
    var files = [
      'starter_kit.breakpoints.yml',
      'starter_kit.info.yml',
      'starter_kit.libraries.yml',
      'config.json'
    ];
    return gulp.src(files)
      .pipe(template(answers))
      .pipe(replace('Starter Kit', answers.name))
      .pipe(replace('starter_kit', answers.machineName))
      .pipe(replace('localhost', answers.devurl))
      .pipe(rename(function (file) {
        if (file.basename.indexOf('starter_kit') === 0) {
          file.basename = file.basename.replace('starter_kit', answers.machineName);
        }
      }))
      .pipe(gulp.dest('./'))
      .pipe(install())
      .on('finish', function () {
        del.sync([
          './starter_kit.breakpoints.yml',
          './starter_kit.info.yml',
          './starter_kit.libraries.yml'
        ]);
        fs.rename('../starter_kit', '../' + answers.machineName);
        done();
      });
  });
});
